function createCard(name, description, pictureUrl) {
    
    
    
    return `
      <div class="col mb-4">
        <div class="card shadow">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <p class="card-text">${description}</p>
          </div>
        </div>
      </div>
    `;
  }
  


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Handle bad response
      } else {
        const data = await response.json();
        const rowContainer = document.getElementById('conferenceRow');
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
  
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            

            const html = createCard(title, description, pictureUrl);
            rowContainer.innerHTML += html;
          }
        }
      }
    } catch (e) {
      // Handle errors
    }
  
  });